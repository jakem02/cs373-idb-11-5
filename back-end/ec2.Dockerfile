FROM python

# update Linux package manager
RUN apt-get update

# line-ending converter
RUN apt-get -y install dos2unix

# GNU bignum library, required by checktestdata
RUN apt-get -y install libboost-dev

# GNU bignum library, required by checktestdata
RUN apt-get -y install libgmp-dev

# editor
RUN apt-get -y install vim

# update Python package manager
RUN pip install --upgrade pip
RUN pip --version
COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt



CMD bash
