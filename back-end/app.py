from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from sqlalchemy import and_, or_, Integer
from sqlalchemy.sql.expression import label, desc, asc


app = Flask(__name__)
CORS(app)
# configure the SQLite database, relative to the app instance folder
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql://admin:Glenn!scool@soccerstars-database.c76dqslr8ztz.us-east-2.rds.amazonaws.com:3306/soccerstars"
app.config["CORS_HEADERS"] = "Content-Type"
# initialize the app with the extension
db = SQLAlchemy(app)


def to_dict(obj):
    return {c.name: getattr(obj, c.name) for c in obj.__table__.columns}


class Athletes(db.Model):
    __tablename__ = "Athletes"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    age = db.Column(db.Integer)
    height = db.Column(db.String)
    weight = db.Column(db.String)
    goals = db.Column(db.Integer)
    fouls = db.Column(db.Integer)
    team = db.Column(db.String, nullable=False)
    country = db.Column(db.String, nullable=False)
    birthplace = db.Column(db.String)
    position = db.Column(db.String)
    nationality = db.Column(db.String)
    team_id = db.Column(db.Integer, nullable=False)
    player_image = db.Column(db.String)
    team_logo = db.Column(db.String)
    country_flag = db.Column(db.String)
    sort_fields = {
        "name",
        "height",
        "weight",
        "age",
        "goals",
        "position",
        "nationality",
    }
    search_fields = {
        "name": 100,
        "team": 50,
        "country": 75,
        "birthplace": 25,
        "nationality": 25,
    }
    per_page = 18


class Teams(db.Model):
    __tablename__ = "Teams"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    wins = db.Column(db.Integer)
    losses = db.Column(db.Integer)
    governing_country = db.Column(db.String)
    total_games = db.Column(db.Integer)
    goals = db.Column(db.Integer)
    goals_avg = db.Column(db.Float, nullable=False)
    goals_against = db.Column(db.Integer)
    league = db.Column(db.String)
    date_founded = db.Column(db.Integer)
    venue = db.Column(db.String)
    venue_image = db.Column(db.String)
    team_logo = db.Column(db.String)
    league_logo = db.Column(db.String)
    country_flag = db.Column(db.String)
    sort_fields = {
        "name",
        "wins",
        "losses",
        "goals",
        "total_games",
        "goals_avg",
        "governing_country",
    }
    search_fields = {
        "name": 100,
        "governing_country": 75,
        "league": 25,
        "venue": 10,
        "date_founded": 5,
    }
    per_page = 18


class Countries(db.Model):
    __tablename__ = "Countries"
    name = db.Column(db.String, primary_key=True)
    native_names = db.Column(db.JSON)
    population = db.Column(db.Integer)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    area = db.Column(db.Float)
    capital = db.Column(db.String)
    region = db.Column(db.String)
    currencies = db.Column(db.JSON)
    languages = db.Column(db.JSON)
    league = db.Column(db.String)
    continent = db.Column(db.String)
    google_maps = db.Column(db.String)
    news_element = db.Column(db.String)
    flag_url = db.Column(db.String)
    sort_fields = {"name", "population", "area", "latitude", "longitude", "region"}
    search_fields = {
        "name": 100,
        "native_names": 50,
        "capital": 50,
        "region": 35,
        "league": 25,
        "continent": 40,
    }
    per_page = 18


import re
import operator


def parse_filtering(model, phrase):
    comparison_tokens = "=|>=|<=|>|<"

    def filter_gen(filters, model):
        for f in filters:
            comparison = None
            if "<=" in f:
                comparison = operator.le
            elif ">=" in f:
                comparison = operator.ge
            elif "<" in f:
                comparison = operator.lt
            elif ">" in f:
                comparison = operator.gt
            elif "=" in f:
                comparison = operator.eq
            if comparison:
                k, v = re.split(comparison_tokens, f)
                yield comparison(getattr(model, k), v)

    filters = [
        x
        for x in phrase.split(",")
        if re.split(comparison_tokens, x)[0] in model.sort_fields
        and len(re.split(comparison_tokens, x)) == 2
    ]
    return filter_gen(filters, model)


def model_multi_query(model, args):
    page = request.args.get("page", default=1, type=int)
    result = model.query

    # filtering
    filter_phrase = args.get("filter", default="", type=str)
    filter_bool = and_(*parse_filtering(model, filter_phrase))
    result = result.filter(filter_bool)

    # sorting
    sort_col = args.get("sort", default=None, type=str)
    if sort_col not in model.sort_fields:
        sort_col = None
    desc = args.get("desc", default="False", type=str)

    if sort_col:
        if desc.lower() == "true":
            result = result.order_by(getattr(model, sort_col).desc())
        else:
            result = result.order_by(getattr(model, sort_col).asc())

    # searching
    search_tokens = args.get("search", default="", type=str).split(",")
    if search_tokens == [""]:
        search_tokens = []

    if search_tokens:
        search_filter_exprs = [
            (
                and_(
                    getattr(model, col).isnot(None),
                    getattr(model, col).ilike("%" + term + "%"),
                )
            ).cast(Integer)
            * weight
            for term in search_tokens
            for col, weight in model.search_fields.items()
        ]
        search_weight = sum(search_filter_exprs)
        result = result.order_by(search_weight.desc()).filter(search_weight > 0)

    # paging
    result = result.paginate(page=page, per_page=model.per_page)
    return {
        "data": [to_dict(team) for team in result.items],
        "paging": {"current": page, "total": result.pages},
        "count": result.total,
    }


@app.route("/athletes")
def get_athletes():
    return jsonify(model_multi_query(Athletes, request.args))


@app.route("/athletes/<id>")
def get_athlete(id):
    athlete = Athletes.query.filter_by(id=id).first()
    return jsonify(to_dict(athlete))


# app_routes for countries and teams need to be tested
@app.route("/teams")
def get_teams():
    return jsonify(model_multi_query(Teams, request.args))


@app.route("/teams/<id>")
def get_team(id):
    team = Teams.query.filter_by(id=id).first()
    team = to_dict(team)
    teamName = team["name"]
    # search the athletes table for an athelte who is part of this team
    playersFromTeam = Athletes.query.filter_by(team=teamName).first()
    playersFromTeam = to_dict(playersFromTeam)
    athleteInfo = {}
    athleteInfo["name"] = playersFromTeam["name"]
    athleteInfo["id"] = playersFromTeam["id"]
    athleteInfo["player_image"] = playersFromTeam["player_image"]
    team["athletes_from_team"] = athleteInfo
    return jsonify(team)


@app.route("/countries")
def get_countries():
    return jsonify(model_multi_query(Countries, request.args))


@app.route("/countries/<name>")
def get_country(name):
    country = Countries.query.filter_by(name=name).first()
    country = to_dict(country)
    countryName = country["name"]
    # search athletes table for an athelete who is from or plays for this country
    playersFromCountry = Athletes.query.filter_by(country=countryName).first()
    playersFromCountry = to_dict(playersFromCountry)
    athleteInfo = {}
    athleteInfo["name"] = playersFromCountry["name"]
    athleteInfo["id"] = playersFromCountry["id"]
    athleteInfo["player_image"] = playersFromCountry["player_image"]
    country["athletes_from_country"] = athleteInfo

    # search the teams table for a team that plays in this country
    teamsFromCountry = Teams.query.filter_by(governing_country=countryName).first()
    teamsFromCountry = to_dict(teamsFromCountry)
    teamInfo = {}
    teamInfo["name"] = teamsFromCountry["name"]
    teamInfo["id"] = teamsFromCountry["id"]
    teamInfo["team_logo"] = teamsFromCountry["team_logo"]
    country["teams_from_country"] = teamInfo

    return jsonify(country)


@app.route("/search")
def search():
    return jsonify(
        {
            "Athletes": model_multi_query(Athletes, request.args),
            "Teams": model_multi_query(Teams, request.args),
            "Country": model_multi_query(Countries, request.args),
        }
    )


@app.route("/")
def hello_world():
    return '<img src="https://thumbs.gfycat.com/DelayedAjarCommongonolek-max-1mb.gif" alt="cowboy" />'


if __name__ == "__main__":
    app.run("0.0.0.0", port=5000, debug=True)
