import requests
import os
import errno
import time

news_url = (
    "https://newsdata.io/api/1/news?apikey=pub_11798e07129df246d5e49573931e5236ebb19"
)
outdir = "results/"


def call(url):
    response = requests.get(news_url + url)
    return response


def ensure_dir(path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass
    return path


countries = {"United_Kingdom": "gb", "France": "fr", "Spain": "es"}


def get_country_news(countries):
    for c in countries:
        d = ensure_dir(outdir + "country_news/")
        f = open(d + c + ".json", "w")
        response = call("&country=" + countries[c] + "&language=en")
        if int(response.json()["totalResults"]) == 0:
            response = call("&country=" + countries[c])
        f.write(response.text)


get_country_news(countries)
