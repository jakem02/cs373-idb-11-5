"""
Based off of implementation in Parkdex IDB Project

Link: https://gitlab.com/maxwthomas/cs373-idb/-/blob/phase2/backend/data/db_init.py
"""
import mysqlinfo
import json

import mysql.connector
from os import listdir
from os.path import isfile, join


def choose_league(league_data, num):
    try:
        result = []
        for league in league_data:
            for s in league["seasons"]:
                cov = s["coverage"]
                if s["year"] == 2021 and cov["players"] and cov["top_scorers"]:
                    result.append((league["league"]["name"], league["league"]["id"]))
        result.sort(key=lambda x: x[1])
        return result[:num]
    except Exception as e:
        return []


def db_connect():
    return mysql.connector.connect(
        host=mysqlinfo.HOST,
        user=mysqlinfo.USER,
        password=mysqlinfo.PASS,
        database=mysqlinfo.DB,
    )


# Creates database if it does not already exist
def create_db():
    with mysql.connector.connect(
        host=mysqlinfo.HOST, user=mysqlinfo.USER, password=mysqlinfo.PASS
    ) as connection:
        database = mysqlinfo.DB
        with connection.cursor() as cursor:
            cursor.execute(f"CREATE DATABASE IF NOT EXISTS {database}")
            connection.commit()


def create_tables(connection):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS Athletes(
                id INT(7) NOT NULL UNIQUE,
                name VARCHAR(255) NOT NULL,
                age INT(7),
                height VARCHAR(20),
                weight VARCHAR(20),
                goals INT(7),
                fouls INT(7),
                team VARCHAR(255) NOT NULL,
                country VARCHAR(255) NOT NULL,
                birthplace VARCHAR(255),
                position VARCHAR(127),
                nationality VARCHAR(255),
                team_id INT(7) NOT NULL,
                player_image VARCHAR(255),
                team_logo VARCHAR(255),
                country_flag VARCHAR(255),
                PRIMARY KEY (id)
            )
            """
        )

        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS Teams(
                id INT(7) NOT NULL UNIQUE,
                name VARCHAR(255) NOT NULL,
                wins INT(7), 
                losses INT(7),
                governing_country VARCHAR(255) NOT NULL,
                total_games INT(7),
                goals INT(7),
                goals_avg FLOAT(24),
                goals_against INT(7),
                league VARCHAR(255) NOT NULL,
                date_founded INT(7),
                venue VARCHAR(255),
                venue_image VARCHAR(255),
                team_logo VARCHAR(255),
                league_logo VARCHAR(255),
                country_flag VARCHAR(255),
                PRIMARY KEY (id)
            )
            """
        )

        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS Countries(
                name VARCHAR(255) NOT NULL UNIQUE,
                native_names JSON,
                population INT(7),
                latitude FLOAT(20) NOT NULL,
                longitude FLOAT(20) NOT NULL,
                area FLOAT(7) NOT NULL,
                capital VARCHAR(255),
                region VARCHAR(255),
                currencies JSON,
                languages JSON,
                league VARCHAR(255),
                continent VARCHAR(255),
                google_maps VARCHAR(255),
                news_element VARCHAR(511),
                flag_url VARCHAR(255),
                PRIMARY KEY (name)
            )
            """
        )

        connection.commit()


def null_check(element):
    if element:
        return element
    else:
        return "0"


def parse_ath(filepath):
    print(filepath)
    f = open(filepath, "r")
    data = json.load(f)
    row = {}
    player = data["player"]
    stats = data["statistics"][0]

    row["id"] = player["id"]
    row["name"] = player["name"]
    row["age"] = int(null_check(player["age"]))
    row["height"] = player["height"]
    row["weight"] = player["weight"]
    row["goals"] = int(null_check(stats["goals"]["total"]))
    row["fouls"] = int(null_check(stats.get("fouls", {"committed": "0"})["committed"]))
    row["team"] = stats["team"]["name"]
    row["country"] = stats["league"]["country"]
    row["birthplace"] = player["birth"]["country"]
    row["position"] = stats["games"]["position"]
    row["nationality"] = player["nationality"]
    row["team_id"] = stats["team"]["id"]
    row["player_image"] = player["photo"]
    row["team_logo"] = stats["team"]["logo"]
    row["country_flag"] = stats["league"]["flag"]
    f.close()
    return row


# parse_team and parse_country need to be tested
def parse_team(filepath):
    print(filepath)
    f = open(filepath, "r")
    data = json.load(f)["response"]
    row = {}
    team = data["team"]
    league = data["league"]
    stats = data["fixtures"]

    row["id"] = team["id"]
    row["name"] = team["name"]
    row["wins"] = int(null_check(stats["wins"]["total"]))
    row["losses"] = int(null_check(stats["loses"]["total"]))
    row["governing_country"] = league["country"]

    row["total_games"] = int(null_check(stats["played"]["total"]))
    row["goals"] = int(null_check(data["goals"]["for"]["total"]["total"]))
    row["goals_avg"] = float(null_check(data["goals"]["for"]["average"]["total"]))
    row["goals_against"] = int(null_check(data["goals"]["against"]["total"]["total"]))
    row["league"] = league["name"]
    row["date_founded"] = int(null_check(league["founded"]))
    row["venue"] = league["venue"]["name"]
    row["venue_image"] = league["venue"]["image"]
    row["team_logo"] = team["logo"]
    row["league_logo"] = league["logo"]
    row["country_flag"] = league["flag"]
    f.close()
    return row


def parse_country(filepath):
    print(filepath)
    try:
        f = open(filepath, "r")
        data = json.load(f)
        row = {}
        stats = data["stats"]
        news = data["news"]
        league = choose_league(data["league"], 1)
        row["name"] = data["league"]["parameters"]["country"]
        row["native_names"] = stats["name"]["nativeName"]
        row["population"] = int(null_check(stats["population"]))  # int
        row["latitude"] = float(null_check(stats["latlng"][0]))  # float
        row["longitude"] = float(null_check(stats["latlng"][1]))  # float
        row["area"] = float(null_check(stats["area"]))  # float
        if type(stats["capital"]) is list:
            row["capital"] = stats["capital"][0]
        else:
            row["capital"] = stats["capital"]
        row["region"] = stats["region"]
        row["currencies"] = stats["currencies"]
        row["languages"] = stats["languages"]
        if league:
            row["league"] = league[0][0]
        row["league"] = "none"
        row["continent"] = stats["continents"][0]
        row["google_maps"] = stats["maps"]["googleMaps"]

        news_res = news.get("results", [{}])
        if not news_res:
            news_res = [{}]
        row["news_element"] = news_res[0].get("link", None)
        row["flag_url"] = stats["flags"]["png"]
        f.close()
        return row
    except KeyError as e:
        print("FAILED", e)
        return {}


def add_athletes(connection, dirpath):

    onlyfiles = (join(dirpath, f) for f in listdir(dirpath) if isfile(join(dirpath, f)))
    athletes = [parse_ath(x) for x in onlyfiles]
    # print(athletes[0])
    with connection.cursor() as cursor:
        cursor.executemany(
            """
                INSERT INTO Athletes(
                    id,
                    name,
                    age,
                    height,
                    weight,
                    goals,
                    fouls,
                    team,
                    country,
                    birthplace,
                    position,
                    nationality,
                    team_id,
                    player_image,
                    team_logo,
                    country_flag
                )
                VALUES(
                    %(id)s,
                    %(name)s,
                    %(age)s,
                    %(height)s,
                    %(weight)s,
                    %(goals)s,
                    %(fouls)s,
                    %(team)s,
                    %(country)s,
                    %(birthplace)s,
                    %(position)s,
                    %(nationality)s,
                    %(team_id)s,
                    %(player_image)s,
                    %(team_logo)s,
                    %(country_flag)s
                )
                """,
            athletes,
        )

        connection.commit()


# add_teams and add_countries need to be tested
def add_teams(connection, dirpath):

    onlyfiles = (join(dirpath, f) for f in listdir(dirpath) if isfile(join(dirpath, f)))
    teams = [parse_team(x) for x in onlyfiles]
    # print(teams[0:2])
    with connection.cursor() as cursor:
        cursor.executemany(
            """
                INSERT INTO Teams(
                    id,
                    name,
                    wins,
                    losses,
                    governing_country,
                    total_games,
                    goals,
                    goals_avg,
                    goals_against,
                    league,
                    date_founded,
                    venue,
                    venue_image,
                    team_logo,
                    league_logo,
                    country_flag

                )
                VALUES(
                    %(id)s,
                    %(name)s,
                    %(wins)s,
                    %(losses)s,
                    %(governing_country)s,
                    %(total_games)s,
                    %(goals)s,
                    %(goals_avg)s,
                    %(goals_against)s,
                    %(league)s,
                    %(date_founded)s,
                    %(venue)s,
                    %(venue_image)s,
                    %(team_logo)s,
                    %(league_logo)s,
                    %(country_flag)s
                )
                """,
            teams,
        )

        connection.commit()


def add_countries(connection, dirpath):

    onlyfiles = (join(dirpath, f) for f in listdir(dirpath) if isfile(join(dirpath, f)))
    countries = [parse_country(x) for x in onlyfiles]

    with connection.cursor() as cursor:
        for country in countries:

            if country:
                country["native_names"] = json.dumps(country["native_names"])
                country["languages"] = json.dumps(country["languages"])
                country["currencies"] = json.dumps(country["currencies"])
                # print(country)
                cursor.execute(
                    """
                        INSERT INTO Countries(
                            name,
                            native_names,
                            population,
                            latitude,
                            longitude,
                            area,
                            capital,
                            region,
                            currencies,
                            languages,
                            league,
                            continent,
                            google_maps,
                            news_element,
                            flag_url
                        
                        )
                        VALUES(
                            %(name)s,
                            %(native_names)s,
                            %(population)s,
                            %(latitude)s,
                            %(longitude)s,
                            %(area)s,
                            %(capital)s,
                            %(region)s,
                            %(currencies)s,
                            %(languages)s,
                            %(league)s,
                            %(continent)s,
                            %(google_maps)s,
                            %(news_element)s,
                            %(flag_url)s
                        )
                        """,
                    country,
                )

        connection.commit()


def fill_tables(connection):
    athlete_dir = "json/athletes/"
    add_athletes(connection, athlete_dir)
    team_dir = "json/teams/"
    add_teams(connection, team_dir)
    country_dir = "json/countries/"
    add_countries(connection, country_dir)


def drop_tables(connection):
    with connection.cursor() as cursor:
        cursor.execute("DROP TABLE IF EXISTS Athletes")
        cursor.execute("DROP TABLE IF EXISTS Teams")
        cursor.execute("DROP TABLE IF EXISTS Countries")
        connection.commit()


def main():
    create_db()
    c = db_connect()
    drop_tables(c)
    create_tables(c)
    print("created table")
    fill_tables(c)
    print("filled tables")


if __name__ == "__main__":
    main()
