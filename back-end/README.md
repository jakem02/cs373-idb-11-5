# How to work with the backend
The backend runs on Flask and primarily relies on app.py. In app.py the table format and routing logic is implemented.

## How to test the flask app
```
cd ~/cs373-IDB-11-5/back-end
#enter the docker images
make run-image

#inside the docker image
python app.py
```
To access the API use this url
```
<url of ec2 instance>:5000/athletes

```
[ec2-3-138-112-84.us-east-2.compute.amazonaws.com:5000/athletes](http://ec2-3-138-112-84.us-east-2.compute.amazonaws.com:5000/athletes)


## How to run the Flask app in the backgroud
`Tmux` is a tool that allows perpetual bash shells to run. This allows you to hop in and out of a shell whenever without killing the processes running within the shell.

Check what tmux processes are running already
```
tmux ls
```

If app is already running
```
tmux attach -t soccerstars
```

To start the tmux session
```
tmux new -s soccerstars
```

Once in the tmux session run docker, run app.py, and then `hit ctrl-b then d` to leave the session
```
make run-image
python app.py
#ctrl-b then d
```



# Command help

## connecting to EC2
```
ssh -i <path to local PEM file> ubuntu@<external ip>
ssh -i ~/.ssh/DanielUbuntu.pem ubuntu@3.143.215.186
ssh -i /Users/dhruvsethi/Desktop/idb-22/DanielUbuntu.pem ubuntu@3.21.52.140
```

## connecting to mySQL from EC2
```
mysql -h <host> -P 3306 -u admin -p
mysql -h soccerstars-database.c76dqslr8ztz.us-east-2.rds.amazonaws.com -P 3306 -u admin -p



#check for tables in soccerstars
mysql> use soccerstars
mysql [soccerstars] > show tables;
```

# Elastic Beanstalk instructions
## online setup instructions
https://github.com/forbesye/cs373/blob/main/Flask_AWS_Deploy.md

## how to deploy
```
cd back-end
git checkout main (or whatever desired branch)
eb deploy soccerstars-prod
```