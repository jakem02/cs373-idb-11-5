import { React, useEffect } from 'react';
import daniel from "../../assets/members/daniel.jpg";
import dhruv from "../../assets/members/dhruv.jpg";
import grant from "../../assets/members/grant.jpg";
import yazan from "../../assets/members/yazan.jpg";
import {Container, Card, Row, Col, Button} from "react-bootstrap";




async function getGitLabInfo () {
    // code derived from https://gitlab.com/catchingcongress/catchingcongress/-/blob/main/frontend/src/pages/about/components/GitLabStats.ts
    let totalCommitCount = 0,
        totalIssueCount = 0,
        totalTestCount = 0;

    let members = [{name: "Daniel Jones", username: "danbotMBM", othername: "daniel", email: "danielmarkjones9@gmail.com",commits: 0, issues: 0, tests: 0, id: "dj"},
        {name: "Grant Shirah", username: "gmshirah", othername: "grant", email: "gmshirah@gmail.com",commits: 0, issues: 0, tests: 0, id: "gs"},
        {name: "Yazan H Al-Sukhni", username: "yalsukhni", othername: "Yazan Al-Sukhni", email: "yalsukhni@utexas.edu",commits: 0, issues: 0, tests: 0, id: "ya"},
        {name: "Dhruv Sethi", username: "dhruvsethi227", othername: "dhruv", email: "dhruvsethi227@gmail.com",commits: 0, issues: 0, tests: 0, id: "ds"}];

    let msg = await fetch("https://gitlab.com/api/v4/projects/39575578/repository/contributors");
    let commitList = await msg.json();
    console.log(commitList)
    commitList.forEach((element) => {
        const { name, email, commits } = element;
        members.forEach((member) => {
            if (member.name === name || member.username === name || member.othername === name || member.email === email) {
                member.commits += commits;
            }
        });
    totalCommitCount += commits;
    });
    
    members.forEach((member) => {
        let dc = document.getElementById(member.id + "c").innerHTML = "<b># of Commits:</b> " + member.commits;
    });
    

    const issuePaginationLength = 100;
    let page = 1;
    let issuePage = [];
    let issueList = [];
    do {
        issuePage = await fetch(
        `https://gitlab.com/api/v4/projects/39575578/issues?per_page=${issuePaginationLength}&page=${page++}`
        );
        issuePage = await issuePage.json();
        console.log(issuePage);
        issueList = [...issueList, ...issuePage];
    } while (issuePage.length === 100);

    issueList.forEach((element) => {
        const { assignees } = element;
        assignees.forEach((a) => {
        const { name } = a;
        members.forEach((member) => {
            if (member.name === name || member.username === name || member.othername === name) {
            member.issues += 1;
            }
        });
        });
        totalIssueCount += 1;
    });

    members.forEach((member) => {
        let dc = document.getElementById(member.id + "i").innerHTML = "<b># of Issues:</b> " + member.issues;
    });
    document.getElementById("tc").innerHTML = "<b>Total # of Commits:</b> " + totalCommitCount;
    document.getElementById("ti").innerHTML = "<b>Total # of Issues:</b> " + totalIssueCount;
};
getGitLabInfo();






function About() {
    
    const linkTo = (route) => {
        window.location.href = route;
    };

    useEffect(() => {
        document.title = `Soccer Stars | About`;
    });

    return (
        <>
            <Container className="pt-5 pb-2">
                <h1>About</h1>
            </Container>
            <Row className="mb-2 justify-content-center">
                <Col md={6} sm={12} className="px-5">
                    <h3>Purpose</h3>
                    <p>Soccer Stars aims to promote engagement with soccer on a global scale, relate teams to the events and successes of their respective countries, and expand recognition of global cultures.</p>
                </Col>
                <Col md={6} sm={12} className="px-5">
                    <h3>Integrating Disparate Data</h3>
                    <p>We combine soccer statistics and world news to be able to understand the enviornments and cultures of our favorite soccer players and teams. This allows us to understand the greater context of the world through soccer. </p>
                </Col>
            </Row>

            <Row className="mb-2">
                <Col lg={3} md={6} xs={12} className="card px-0">
                    <Card.Img
                        variant="top"
                        className="w-100"
                        src={daniel}
                        alt="Daniel"
                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '20vw', minHeight: '400px'}}
                    />
                    <Card.Body className="d-flex flex-column">
                        <Card.Title className="mb-auto">
                            <h4>Daniel Jones</h4>
                        </Card.Title>
                        <Card.Text className="mb-auto">
                        <p>Daniel is a UTCS senior who's main interest is AI, autonomy, and robotics. He enjoys rock climbing, rocket league, and sports with friends.</p>
                        </Card.Text>
                        <Card.Text className="mb-0">
                        <p className="mb-0"><b>Major Responsibilities</b></p>
                        </Card.Text>
                        
                        <ul className="mx-auto" style={{textAlign: 'left'}}>
                            <li>
                                <p className="mb-0">Backend</p>
                            </li>
                            <li>
                                <p className="mb-0">Cloud Hosting</p>
                            </li>
                        </ul>
                        <Card.Text className="mb-0" id="djc"><p><b># of Commits:</b> X</p></Card.Text>
                        <Card.Text className="mb-0" id="dji"><p><b># of Issues:</b> X</p></Card.Text>
                        <Card.Text><p><b># of Unit Tests:</b> 0</p></Card.Text>

                        <Button variant="primary" href="https://gitlab.com/danbotMBM">GitLab Profile</Button>
                    </Card.Body>
                </Col>
                <Col lg={3} md={6} xs={12} className="card px-0">
                    <Card.Img
                        variant="top"
                        className="w-100"
                        src={dhruv}
                        alt="Dhruv"
                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '20vw', minHeight: '400px'}}
                    />
                    <Card.Body className="d-flex flex-column">
                        <Card.Title className="mb-auto">
                            <h4>Dhruv Sethi</h4>
                        </Card.Title>
                        <Card.Text className="mb-auto">
                        <p>Dhruv is a senior studying computer science and business. His interests include fin-tech, soccer, and spending time with friends and family.</p>
                        </Card.Text>
                        <Card.Text className="mb-0">
                        <p className="mb-0"><b>Major Responsibilities</b></p>
                        </Card.Text>
                        <ul className="mx-auto" style={{textAlign: 'left'}}>
                            <li>
                            <p className="mb-0">Backend</p>
                            </li>
                            <li>
                            <p className="mb-0">Postman</p>
                            </li>
                        </ul>
                        <Card.Text className="mb-0" id="dsc"><p><b># of Commits:</b> X</p></Card.Text>
                        <Card.Text className="mb-0" id="dsi"><p><b># of Issues:</b> X</p></Card.Text>
                        <Card.Text><p><b># of Unit Tests:</b> 0</p></Card.Text>

                        <Button variant="primary" href="https://gitlab.com/dhruvsethi227">GitLab Profile</Button>
                    </Card.Body>
                </Col>
                <Col lg={3} md={6} xs={12} className="card px-0">
                    <Card.Img
                        variant="top"
                        className="w-100"
                        src={grant}
                        alt="Grant"
                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '20vw', minHeight: '400px'}}
                    />
                    <Card.Body className="d-flex flex-column">
                        <Card.Title className="mb-auto">
                            <h4>Grant Shirah</h4>
                        </Card.Title>
                        <Card.Text className="mb-auto">
                        <p>Grant is a senior studying computer science, interested in software engineering. He enjoys cooking, exploring new cities, and spending time with friends.</p>
                        </Card.Text>
                        <Card.Text className="mb-0">
                        <p className="mb-0"><b>Major Responsibilities</b></p>
                        </Card.Text>
                        <ul className="mx-auto" style={{textAlign: 'left'}}>
                            <li>
                            <p className="mb-0">Frontend</p>
                            </li>
                            <li>
                            <p className="mb-0">GitLab Issues</p>
                            </li>
                        </ul>
                        <Card.Text className="mb-0" id="gsc"><p><b># of Commits:</b> X</p></Card.Text>
                        <Card.Text className="mb-0" id="gsi"><p><b># of Issues:</b> X</p></Card.Text>
                        <Card.Text><p><b># of Unit Tests:</b> 0</p></Card.Text>

                        <Button variant="primary" href="https://gitlab.com/gmshirah">GitLab Profile</Button>
                    </Card.Body>
                </Col>
                <Col lg={3} md={6} xs={12} className="card px-0">
                    <Card.Img
                        variant="top"
                        className="w-100"
                        src={yazan}
                        alt="Yazan"
                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', height: '20vw', minHeight: '400px'}}
                    />
                    <Card.Body className="d-flex flex-column">
                        <Card.Title className="mb-auto">
                            <h4>Yazan Alsukhni</h4>
                        </Card.Title>
                        <Card.Text className="mb-auto">
                        <p>Yazan is a 4th-year computer science major, in his free time he likes to work on his car, lift weights, and pursue artistic endeavors.</p>
                        </Card.Text>
                        <Card.Text className="mb-0">
                        <p className="mb-0"><b>Major Responsibilities</b></p>
                        </Card.Text>
                        <ul className="mx-auto" style={{textAlign: 'left'}}>
                            <li>
                            <p className="mb-0">Customer Relations</p>
                            </li>
                            <li>
                            <p className="mb-0">Documentation</p>
                            </li>
                        </ul>
                        <Card.Text className="mb-0" id="yac"><p><b># of Commits:</b> X</p></Card.Text>
                        <Card.Text className="mb-0" id="yai"><p><b># of Issues:</b> X</p></Card.Text>
                        <Card.Text><p><b># of Unit Tests:</b> 0</p></Card.Text>

                        <Button variant="primary" href="https://gitlab.com/yalsukhni">GitLab Profile</Button>
                    </Card.Body>
                </Col>
            </Row>

            
            <h2>GitLab</h2>
            <p className="mb-0" id="tc"><b>Total # of Commits:</b> X</p>
            <p className="mb-0" id="ti"><b>Total # of Issues:</b> X</p>
            <p><b>Total # of Unit Tests:</b> 0</p>
            
            <Container>
                <Row className="mx-auto">
                    <h2>Data</h2>
                </Row>
                <Row className="justify-content-center">
                    <Col md="auto">
                        <ul className="mx-auto" style={{textAlign: 'left'}}>
                            <li>
                                <p className="mb-0"><a href="https://restcountries.com/">Country API</a> where we scrape for facts and figures about each country.</p>
                            </li>
                            <li>
                                <p className="mb-0"><a href="https://www.api-football.com/">Soccer API</a> where we scrape for all the stats on your favorite teams and players.</p>
                
                            </li>
                            <li>
                                <p className="mb-0"><a href="https://newsdata.io/">News API</a> where we scrape for current events in the regions related to each instance.</p>
                
                            </li>
                        </ul>
                    </Col>
                </Row>
            </Container>
            <Container>
                <Row className="mx-auto">
                    <h2>Tools</h2>
                </Row>
                <Row className="justify-content-center">
                    <Col md="auto">
                        <ul className="mx-auto" style={{textAlign: 'left'}}>
                            <li>
                                <p className="mb-0"><a href="https://www.postman.com/">Postman:</a> An API platform used to design, build, test, and iterate over APIs. For phase
                                    one, we are using Postman to host the documentation for our API.
                                    </p>
                            </li>
                            <li>
                                <p className="mb-0"><a href="https://www.namecheap.com/?gclid=Cj0KCQjwkOqZBhDNARIsAACsbfLCBHCC3RP85j0BaSwWo5rRDTLk2DjWUg1IbBf7YsN9uY8LxTvuBpMaAgaGEALw_wcB">Namecheap:</a> A domain name registrar we used to register a free domain for our website.</p>
                
                            </li>
                            <li>
                                <p className="mb-0"><a href="https://aws.amazon.com/amplify/?trk=41731cf6-f5eb-4611-81ef-f2914ec706b5&sc_channel=ps&s_kwcid=AL!4422!3!588971138365!e!!g!!aws%20amplify&ef_id=Cj0KCQjwkOqZBhDNARIsAACsbfK9Dz0nPzckJlE5S3xN5NDEowxb_Imlw3wkkKvlkg0Vazvy3TzVsqoaAhRfEALw_wcB:G:s&s_kwcid=AL!4422!3!588971138365!e!!g!!aws%20amplify">AWS Amplify:</a> AWSs web hosting service. This is what we used to host our website.</p>
                
                            </li>
                            <li>
                                <p className="mb-0"><a href="https://about.gitlab.com/">GitLab:</a> A web-based version control tool that enables source code management,
                                    development, and collaboration. GitLab offers a complete suite of DevOps tools such as
                                    a CI/CD pipeline and issue tracking. Our team used GitLab to hold all our source code,
                                    automatically build our code, and create issues to track tasks and bugs for our project.
                                    </p>
                
                            </li>
                            <li>
                                <p className="mb-0"><a href="https://discord.com/">Discord:</a> A popular communication platform that our team used to coordinate meeting times, tasking, and actually holding the meetings there.</p>
                
                            </li>
                            <li>
                                <p className="mb-0"><a href="https://html-validate.org/usage/cli.html">html-validate:</a> This is a CLI tool that enforces a certain style and best practices for html, css, and javascript files.</p>
                
                            </li>
                        </ul>
                    </Col>
                </Row>
            </Container>
            <h2 className="mx-auto">Links</h2>
            <Container className="mb-3">
                <Button size="lg" variant="outline-primary" className="m-2" href="https://gitlab.com/danbotMBM/cs373-idb-11-5">GitLab Repository</Button>
                <Button size="lg" variant="outline-primary" className="m-2" href="https://documenter.getpostman.com/view/17915497/2s83tJGWDZ">Link to Postman API</Button>
            </Container>
        </> 
    );

    

}
export default About;
