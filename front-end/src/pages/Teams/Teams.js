import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Row, Col, Container, Card, CardBody, CardImg, Button,
    Pagination, PaginationItem, PaginationLink, Input } from 'reactstrap';
import { Highlight } from "react-highlighter-ts";

function Teams() {

    const teamsAPI = `https://api.soccerstars.me${window.location.pathname}`;

    const [teamData, setTeamData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [tryAgain, setTryAgain] = useState(false);
    const [query, setQuery] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const [sortBy, setSortBy] = useState(null);
    const [desc, setDesc] = useState(false);
    const [filterBy, setFilterBy] = useState(null);
    const [filterOp, setFilterOp] = useState("=");
    const [filterAmt, setFilterAmt] = useState(null);

    useEffect(() => {
        document.title = `Soccer Stars | Teams`;
    });

    useEffect(() => {
        setLoading(true);
        // let isSubscribed = true
        // console.log(isSortAscending)
        // if(isSortAscending) {
        //     setDesc(null)
        // } else {
        //     setDesc(true)
        // }
        axios.get(teamsAPI, {
            params: {
                page: currentPage,
                search: query,
                sort: sortBy,
                desc: desc,
                filter: "" + filterBy + filterOp + filterAmt
            }
        })
            .then((response) => {
                setTeamData(response.data);
            })
            .catch(function (error) {
                console.error(error);
            })
            .then(function () {
                setLoading(false);
                setTryAgain(false);
            });
        // return () => isSubscribed = false
    }, [teamsAPI, tryAgain, query, currentPage, sortBy, desc, filterBy, filterOp, filterAmt]);

    const handleQueryChange = (e) => {
        if (e.target.value === '') {
            setQuery(null)
        } else {
            setQuery(e.target.value);
        }
    };

    const handleSortChange = (e) => {
        if (e.target.value === '') {
            setSortBy(null)
        } else {
            setSortBy(e.target.value)
        }
    };

    const handleDescChange = (e) => {
        if (e.target.value == 0) {
            setDesc(false)
        } else {
            setDesc(e.target.value)
        }
    };

    const handleFilterChange = (e) => {
        if (e.target.value === '') {
            setFilterBy(null)
        } else {
            setFilterBy(e.target.value)
        }
    };

    const handleFilterOpChange = (e) => {
        setFilterOp(e.target.value)
    };

    const handleFilterAmtChange = (e) => {
        if (e.target.value === '') {
            setFilterAmt(null)
        } else {
            setFilterAmt(e.target.value)
        }
    };

    if (!teamData || !(teamData.data)) return (
        <div>
            <Container className="p-5">
                <h1>Teams</h1>
                <p>0 total entries</p>
                <p><b>Failed to fetch data array. Press the button to try again...</b></p>
                <Button variant="primary" onClick={() => setTryAgain(true)}>Try Again</Button>
            </Container>
        </div>
    );

    // const paginateFunc = (pageNumber) => setCurrentPage(pageNumber);
    
    return (
        <div>
            <Container className="py-5">
                <h1>Teams</h1>
                <p className="mb-0">{teamData.count} total entries</p>
                <p>{teamData.paging.total} total pages</p>
                {/* <p className="mb-0"><u><b>Team Data from API</b> ({teamsAPI})</u></p>
                <p>{JSON.stringify(teamData.data)}</p> */}
                <Input
                    type="search"
                    placeholder="Search"
                    onChange={handleQueryChange}
                    value={query}
                />
            </Container>
            <Container className="mb-5">
                <p><b>Sort</b></p>
                <Row className="mb-3">
                    <Col>
                        <Input
                            type="select"
                            onChange={handleSortChange}
                            value={sortBy}
                        >
                            <option value="">None</option>
                            <option value="name">Name</option>
                            <option value="wins">Wins</option>
                            <option value="losses">Losses</option>
                            <option value="goals">Goals</option>
                            <option value="total_games">Games</option>
                            <option value="goals_avg">Average Goals/Game</option>
                            <option value="governing_country">Country</option>
                        </Input>
                    </Col>
                    <Col>
                        <Input
                            type="select"
                            onChange={handleDescChange}
                            value={desc}
                        >
                            <option value={0}>Ascending</option>
                            <option value={true}>Descending</option>
                        </Input>
                    </Col>
                </Row>
                <p><b>Filter</b></p>
                <Row className="mb-3">
                    <Col>
                        <Input
                            type="select"
                            onChange={handleFilterChange}
                            value={filterBy}
                        >
                            <option value="">None</option>
                            <option value="wins">Wins</option>
                            <option value="losses">Losses</option>
                            <option value="goals">Goals</option>
                            <option value="total_games">Games</option>
                            <option value="goals_avg">Average Goals/Game</option>
                        </Input>
                    </Col>
                    <Col>
                        <Input
                            type="select"
                            onChange={handleFilterOpChange}
                            value={filterOp}
                        >
                            <option value="=">Equal To</option>
                            <option value=">">Greater Than</option>
                            <option value="<">Less Than</option>
                        </Input>
                    </Col>
                    <Col>
                        <Input
                            type="number"
                            placeholder="Value"
                            onChange={handleFilterAmtChange}
                            value={filterAmt}
                        />
                    </Col>
                </Row>
            </Container>
            <Container className="pb-2">
                <p>{sortBy == null ? "" : `Currently sorted by ${sortBy}${desc ? " descending" : ""}`}</p>
            </Container>
            <Container>
                { loading ? (
                    <h2>Loading...</h2>
                ) : (
                    <Row xs={2} sm={3} md={4} lg={6} className="mb-5 justify-content-center">
                        {(teamData.data).map((team) => (
                            <Card className="px-0">
                                <CardImg 
                                    src={team.team_logo}
                                    className="m-auto"
                                    style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', maxHeight: '200px', maxWidth: '200px'}}
                                />
                                <CardBody className="d-flex flex-column">
                                    <h4 className="mb-auto"><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{team.name}</Highlight></h4>
                                    <p className="mb-0"><u><b>Country</b></u></p>
                                    <p className="mb-0"><Highlight search={query} matchStyle={{backgroundColor: 'orange'}}>{team.governing_country}</Highlight></p>
                                    <p className="mb-0"><u><b>Games</b></u></p>
                                    <p className="mb-0">{team.total_games}</p>
                                    <p className="mb-0"><u><b>Wins</b></u></p>
                                    <p className="mb-0">{team.wins}</p>
                                    <p className="mb-0"><u><b>Losses</b></u></p>
                                    <p className="mb-0">{team.losses}</p>
                                    <p className="mb-0"><u><b>Win Percentage</b></u></p>
                                    <p className="mb-0">{team.total_games == 0 ? "0" : Math.round(100 * (team.wins / team.total_games))}%</p>
                                    <p className="mb-0"><u><b>Goals</b></u></p>
                                    <p className="mb-0">{team.goals}</p>
                                    <p className="mb-0"><u><b>Average Goals/Game</b></u></p>
                                    <p className="mb-0">{team.goals_avg}</p>
                                    <p className="mb-0"><u><b>Goals Against</b></u></p>
                                    <p>{team.goals_against}</p>
                                    {/* <p><b>ID:</b> {team.id}</p> */}
                                    <Button variant="primary" href={`/teams/${team.id}`}>More Info</Button>
                                </CardBody>
                            </Card>
                        ))}
                    </Row>
                )}
            </Container>
            <Pagination className="mb-5 d-flex justify-content-center">
                <PaginationItem disabled={teamData.paging.current == 1}>
                    <PaginationLink
                        first
                        onClick={() => setCurrentPage(1)}
                    />
                </PaginationItem>
                <PaginationItem disabled={teamData.paging.current == 1}>
                    <PaginationLink
                        previous
                        onClick={() => setCurrentPage(teamData.paging.current - 1)}
                    />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#">
                        {teamData.paging.current}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem disabled={teamData.paging.current == teamData.paging.total}>
                    <PaginationLink
                        next
                        onClick={() => setCurrentPage(teamData.paging.current + 1)}
                    />
                </PaginationItem>
                <PaginationItem disabled={teamData.paging.current == teamData.paging.total}>
                    <PaginationLink
                        last
                        onClick={() => setCurrentPage(teamData.paging.total)}
                    />
                </PaginationItem>
            </Pagination>
        </div>
    );
}
export default Teams;
