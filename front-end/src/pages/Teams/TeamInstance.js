import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Row, Col, Container, Button } from 'reactstrap';

function TeamInstance() {

    const teamsAPI = `https://api.soccerstars.me${window.location.pathname}`;

    const [teamData, setTeamData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [tryAgain, setTryAgain] = useState(false);

    useEffect(() => {
        setLoading(true);
        // let isSubscribed = true
        // console.log(isSortAscending)
        // if(isSortAscending) {
        //     setDesc(null)
        // } else {
        //     setDesc(true)
        // }
        axios.get(teamsAPI)
            .then((response) => {
                setTeamData(response.data);
            })
            .catch(function (error) {
                console.error(error);
            })
            .then(function () {
                setLoading(false);
                setTryAgain(false);
            });
        // return () => isSubscribed = false
    }, [teamsAPI, tryAgain]);

    useEffect(() => {
        document.title = `Soccer Stars | Teams | ${teamData.name}`;
    });

    if (!teamData) return (
        <div>
            <Container className="p-5">
                <h1>Teams</h1>
                <p><b>Failed to fetch data array. Press the button to try again...</b></p>
                <Button variant="primary" onClick={() => setTryAgain(true)}>Try Again</Button>
            </Container>
        </div>
    );
    
    return (
        <div>
            { loading ? (
                <h2>Loading...</h2>
            ) : (
                <div>
                    <Container className="py-5">
                        <h1>{teamData.name}</h1>
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Logo</h2>
                        <img
                            src={teamData.team_logo}
                            style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '400px'}}
                        />
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Stats</h2>
                        <p className="mb-0"><b>Date Founded:</b> {teamData.date_founded}</p>
                        <p className="mb-0"><b>Venue:</b> {teamData.venue}</p>
                        <p className="mb-0"><b>League:</b> {teamData.league}</p>
                        <p className="mb-0"><b>Goals:</b> {teamData.goals}</p>
                        <p className="mb-0"><b>Goals Against:</b> {teamData.goals_against}</p>
                        <p className="mb-0"><b>Average Goals/Game:</b> {teamData.goals_avg}</p>
                        <p className="mb-0"><b>Wins:</b> {teamData.wins}</p>
                        <p className="mb-0"><b>Losses:</b> {teamData.losses}</p>
                        <p className="mb-0"><b>Win Percentage:</b> {teamData.total_games == 0 ? "0" : Math.round(100 * (teamData.wins / teamData.total_games))}%</p>
                        <p className="mb-0"><b>Games:</b> {teamData.total_games}</p>
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Connections</h2>
                        <Row className="justify-content-center">
                            <Col md={6} sm={12} className="px-5">
                                <h3>{teamData.athletes_from_team != null ? teamData.athletes_from_team.name : ""}</h3>
                                <div>
                                    <img
                                        className="mb-3"
                                        src={teamData.athletes_from_team != null ? teamData.athletes_from_team.player_image : ""}
                                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '200px'}}
                                    />
                                </div>
                                <Button className="mb-3" variant="primary" href={teamData.athletes_from_team != null ? `/athletes/${teamData.athletes_from_team.id}` : ""}>More Info</Button>
                            </Col>
                            <Col md={6} sm={12} className="px-5">
                                <h3>{teamData.governing_country}</h3>
                                <div>
                                    <img
                                        className="mb-3"
                                        src={teamData.country_flag}
                                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '400px'}}
                                    />
                                </div>
                                <Button className="mb-3" variant="primary" href={`/countries/${teamData.governing_country}`}>More Info</Button>
                            </Col>
                        </Row>
                    </Container>
                    <Container className="mb-5">
                        <h2 className="mb-3">Additional Attributes</h2>
                        <Row className="justify-content-center">
                        <Col md={6} sm={12} className="px-5">
                                <h3>{teamData.league}</h3>
                                <div>
                                    <img
                                        className="mb-3"
                                        src={teamData.league_logo}
                                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '200px'}}
                                    />
                                </div>
                            </Col>
                            <Col md={6} sm={12} className="px-5">
                                <h3>{teamData.venue}</h3>
                                <div>
                                    <img
                                        className="mb-3"
                                        src={teamData.venue_image}
                                        style={{objectFit: 'cover', objectPosition: 'center', overflow: 'hidden', width: '100%', maxWidth: '400px'}}
                                    />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
            )}
        </div>
    );
}
export default TeamInstance;
