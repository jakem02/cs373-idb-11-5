import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import About from './pages/About/About.js';
import Home from './pages/Home/Home.js';
import Countries from './pages/Countries/Countries.js';
import Athletes from './pages/Athletes/Athletes.js';
import Teams from './pages/Teams/Teams.js';
import Search from './pages/Search/Search.js';
import NavigationBar from './components/NavBar';
import CountryInstance from './pages/Countries/CountryInstance';
import AthleteInstance from './pages/Athletes/AthleteInstance';
import TeamInstance from './pages/Teams/TeamInstance';



function App() {
  return (
       <>
          <div className="App">
              <NavigationBar></NavigationBar>
              <Routes>
                  <Route id='NavHome' exact path='/' element={<Home />} />
                   <Route id='NavAbout' exact path='/about' element={<About />} />
                   <Route id='NavCountries' exact path='/countries' element={<Countries />} />
                   <Route id='NavAthletes' exact path='/athletes' element={<Athletes />} />
                   <Route id='NavTeams' exact path='/teams' element={<Teams />} />
                   <Route id='NavSearch' exact path='/search' element={<Search />} />
                   <Route path='/countries/:id' element={<CountryInstance />} />
                   <Route path='/athletes/:id' element={<AthleteInstance />} />
                   <Route path='/teams/:id' element={<TeamInstance />} />
               </Routes>
           </div >
       </>
  );
}


export default App;
