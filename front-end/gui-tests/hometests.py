#based on https://gitlab.com/Nathaniel-Nemenzo/getthatbread/-/blob/main/frontend/getthatbread/selenium/test_gui.py

import sys
import time
import pytest

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Remote
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = None
wait = None
local = False # set to FALSE when pushing to gitlab

url = "https://www.soccerstars.me/"

def setup_module():
    print("beginning setup for test_gui module")

    # allow gitlab ci/cd to run selenium tests
    global driver, wait
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("window-size=1200x600")
    if local:
        driver = webdriver.Chrome(service = Service(ChromeDriverManager().install()), chrome_options = chrome_options)
    else:
        driver = Remote(
            "http://selenium__standalone-chrome:4444/wd/hub",
            desired_capabilities=chrome_options.to_capabilities(),
        )
    driver.get(url)
    wait = WebDriverWait(driver, 20)
    self.timeout = 5
    return driver

def teardown_module():
    print("tearing down test_gui module")
    driver.quit()




def test_navbar_home():
    print("starting test_navbar_home")
    test = driver.find_element(By.ID, "NavHome")
    test.click()
    assert driver.current_url == url + ""

def test_navbar_about():
    print("starting test_navbar_about")
    test = driver.find_element(By.ID, "NavAbout")
    test.click()
    assert driver.current_url == url + "/about"

def test_navbar_teams():
    print("starting test_navbar_teams")
    test = driver.find_element(By.ID, "NavTeams")
    test.click()
    assert driver.current_url == url + "/teams"

def test_navbar_countries():
    print("starting test_navbar_countries")
    test = driver.find_element(By.ID, "NavCountries")
    test.click()
    assert driver.current_url == url + "/countries"

def test_navbar_athletes():
    print("starting test_navbar_athletes")
    test = driver.find_element(By.ID, "NavAthletes")
    test.click()
    assert driver.current_url == url + "/athletes"



def test_button_aboutUs():
    print("starting test_button_aboutUs")
    test = driver.find_element(By.ID, "AboutUsButton")
    test.click()
    assert driver.current_url == url + "/about"

def test_button_viewCountries():
    print("starting test_button_viewCountries")
    test = driver.find_element(By.ID, "ViewCountriesButton")
    test.click()
    assert driver.current_url == url + "/countries"

def test_button_viewTeams():
    print("starting test_button_viewTeams")
    test = driver.find_element(By.ID, "ViewTeamsButton")
    test.click()
    assert driver.current_url == url + "/teams"

def test_button_viewAthletes():
    print("starting test_button_viewAthletes")
    test = driver.find_element(By.ID, "ViewAthletesButton")
    test.click()
    assert driver.current_url == url + "/athletes"

def test_carousel_loads():
        try:
            WebDriverWait(driver, timeout).until(
                EC.presence_of_element_located((By.ID, "ImageCarousel"))
            )
        except:
            self.fail("carousel not found")
