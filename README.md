# cs373-idb-11-5

## Website
https://www.soccerstars.me/

## Develop url
https://develop.d36n1ulz5li55j.amplifyapp.com/

## Canvas group
11 AM, Group 5

## Names
| Name           | EID     | GitLab ID              |
|----------------|---------|------------------------|
| Daniel Jones   | dmj2378 | danbotMBM              |
| Yazan Alsukhni | yha225  | yalsukhni              |
| Grant Shirah   | gms2872 | gmshirah               | 
| Dhruv Sethi    | ds53434 | dhruvsethi227          | 

## Project Name
soccerstars.me

## Project Leader
Dhruv Sethi - organazied and distributed tasks

## Git SHA
ec260f5799892a93adec5c1c5ecab80693a3353b

## Git pipelines
https://gitlab.com/danbotMBM/cs373-idb-11-5/-/pipelines

## Completion time
| Name           | Estimated Time | Actual Time |
|----------------|----------------|-------------|
| Daniel Jones   | 20 hrs         | 28 hrs      |
| Yazan Alsukhni | 19 hrs         | 21 hrs      |
| Grant Shirah   | 22 hrs         | 26 hrs      | 
| Dhruv Sethi    | 16 hrs         | 20 hrs      | 
